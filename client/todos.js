import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { check } from 'meteor/check';

import './todos.html';

Template.todos.helpers({
	tasks: function() {
    	return Tasks.find({}, { sort: { createdAt: -1 } });
	},

	isOwner: function() {
    	return this.owner === Meteor.userId();
  	}
});

Template.todos.events = {
  	'click .delete'() {
  		Meteor.call('tasks.remove', this._id);
  	},
}

Template.input.events = {
	'keydown input#task' : function (event) {
	    if (event.which == 13) {
	    	event.preventDefault();
	    	var task = document.getElementById('task');
	    	if (task.value != ''){
		    	Meteor.call('tasks.insert', task.value);
		    	document.getElementById('task').value = '';
		    	task.value = '';
	    	}
		}
	},
}