
Meteor.publish('tasks', function() {
  return Tasks.find({}, { sort: { createdAt: -1 } });
});