import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.startup(() => {
  // code to run on server at startup
});

 

Meteor.methods({
  'tasks.insert'(text) {
    check(text, String);
    Tasks.insert({
      text,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
    });
  },

  'tasks.remove'(taskId){
  	check(taskId, String);
  	Tasks.remove(taskId);
  }
});
